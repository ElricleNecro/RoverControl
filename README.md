# RoverControl

This is my version of the challenge proposed [here](https://code.google.com/archive/p/marsrovertechchallenge/).

To see how it performs, run
```
$ cargo test
```
