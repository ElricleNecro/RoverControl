#![no_std]

#[derive(PartialEq,Debug)]
struct Pos<T> {
    x: T,
    y: T
}

struct Terrain {
    upleft: Pos<u64>,
}

impl Terrain {
    pub extern fn new(x: u64, y: u64) -> Terrain {
        Terrain { upleft: Pos { x: x, y: y } }
    }
}

#[derive(PartialEq,Debug)]
pub enum Direction {
    North,
    East,
    South,
    West
}

pub struct Rover {
    pos: Pos<u64>,
    face: Direction,
    plateau: Terrain,
}

impl Rover {
    pub extern fn new(x: u64, y: u64) -> Rover {
        Rover {
            pos: Pos { x: x / 2, y: y / 2 },
            face: Direction::North,
            plateau: Terrain::new(x, y)
        }
    }

    pub fn deploy(&mut self, x: u64, y: u64, dir: Direction) {
        self.pos.x = x;
        self.pos.y = y;
        self.face = dir;
    }

    pub fn turn_left(&mut self) {
        self.face = match self.face {
            Direction::North => Direction::West,
            Direction::East => Direction::North,
            Direction::South => Direction::East,
            Direction::West => Direction::South,
        }
    }

    pub fn turn_right(&mut self) {
        self.face = match self.face {
            Direction::North => Direction::East,
            Direction::East => Direction::South,
            Direction::South => Direction::West,
            Direction::West => Direction::North,
        }
    }

    pub fn go(&mut self) {
        match self.face {
            Direction::North => self.pos.y += 1,
            Direction::East => self.pos.x += 1,
            Direction::South => self.pos.y -= 1,
            Direction::West => self.pos.x -= 1,
        }
    }
}

pub fn run(p_x:u64, p_y: u64, r_x: u64, r_y: u64, dir: Direction, inst: &[&str]) -> (u64, u64, Direction) {
    let mut rover = Rover::new(p_x, p_y);
    rover.deploy(r_x, r_y, dir);

    for &v in inst {
        match v {
            "L" => rover.turn_left(),
            "R" => rover.turn_right(),
            "M" => rover.go(),
            _ => panic!("Unknown instruction"),
        }
    }

    (rover.pos.x, rover.pos.y, rover.face)
}

#[cfg(test)]
mod tests {
    use super::Rover;
    use super::Direction;
    use super::run;
    use super::Pos;

    #[test]
    fn creation() {
        let rover = Rover::new(5, 5);

        assert_eq!(rover.plateau.upleft, Pos { x: 5, y: 5 }, "Wrong coordinate of plateau.");
        assert_eq!(rover.pos, Pos { x: 5 / 2, y: 5 / 2 }, "Wrong coordinate of the rover.");
        assert_eq!(rover.face, Direction::North, "Wrong Direction.");
    }

    #[test]
    fn deploy() {
        let mut rover = Rover::new(5, 5);

        rover.deploy(3, 3, Direction::South);

        assert_eq!(rover.pos, Pos { x: 3, y: 3 }, "Wrong rover position.");
        assert_eq!(rover.face, Direction::South, "Rover does not look in the correct direction.");
    }

    #[test]
    fn turn_left() {
        let mut rover = Rover::new(5, 5);

        rover.deploy(3, 3, Direction::South);
        rover.turn_left();

        assert_eq!(rover.pos, Pos { x: 3, y: 3 }, "Wrong rover position.");
        assert_eq!(rover.face, Direction::East, "Rover does not look in the correct direction.");
    }

    #[test]
    fn turn_right() {
        let mut rover = Rover::new(5, 5);

        rover.deploy(3, 3, Direction::South);
        rover.turn_right();

        assert_eq!(rover.pos, Pos { x: 3, y: 3 }, "Wrong rover position.");
        assert_eq!(rover.face, Direction::West, "Rover does not look in the correct direction.");
    }

    #[test]
    fn go() {
        let mut rover = Rover::new(5, 5);

        rover.deploy(3, 3, Direction::South);
        rover.go();

        assert_eq!(rover.pos, Pos { x: 3, y: 2 }, "Wrong rover position.");
        assert_eq!(rover.face, Direction::South, "Rover does not look in the correct direction.");
    }

    #[test]
    fn run1() {
        let (x, y, dir) = run(5, 5, 1, 2, Direction::North, &[ "L", "M", "L", "M", "L", "M", "L", "M", "M" ]);

        assert_eq!(Pos { x: x, y: y }, Pos { x: 1, y: 3 }, "Wrong final position.");
        assert_eq!(dir, Direction::North, "Wrong heading.");
    }

    #[test]
    fn run2() {
        let (x, y, dir) = run(5, 5, 3, 3, Direction::East, &["M", "M", "R", "M", "M", "R", "M", "R", "R", "M"]);

        assert_eq!(Pos { x: x, y: y }, Pos { x: 5, y: 1 }, "Wrong final position.");
        assert_eq!(dir, Direction::East, "Wrong Heading.");
    }
}
